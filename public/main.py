import random
import math

from browser import console
from browser import window
import javascript


Phaser = window.Phaser

MESSAGES = {
    "Lore1": [
        "Razorvale Isle",
        "-",
        "Prison of the Spires",
        "Tomb of Magic",
    ],
    "Lore2": [
        "Let this be a warning",
        "-",
        "Leave this island now",
    ],
    "Lore3": [
        "The power contained within",
        "Should only be released",
        "If the need is great",
    ],
    "Lore4": [
        "It is not right for anyone",
        "to control this much power",
        "-King Malbar II",
    ],
    "Bones": [
        "These bones look shattered",
        "what could do such a thing?",
    ],
    "King": [
        "The other castaways are exploring",
        "I'm too old to do that",
        "I'll just stay here and fish",
    ],
    "Entrance": [
        "The stone building beyond must be",
        "where the other prisoners went",
        "... but is it safe to enter?",
    ],
    "Instructions1": [
        "Arrow Keys to Move",
        "-",
        "Move Right",
    ],
    "Instructions2": [
        "The Kingdom of Abinbar is in turmoil",
        "It rules over the The Citadel Spires",
        "King Malbar the IX is unpopular",
    ],
    "Instructions3": [
        "He has begun arresting dissidents",
        "He charges them with treason",
        "and casts them away to Razorvale Isle",
    ],
    "Instructions4": [
        "Razorvale Isle is said to be deadly",
        "Exile there is a death sentence",
        "You don't know why you were arrested",
    ],
    "TotemInfo": [
        "A glowing wooden totem",
        "It seems to be sapping the heat",
        "from the air around you",
    ],
    "FlameInfo": [
        "The flames surround you",
        "You are imbued with power",
    ],
    "GlassesInfo": [
        "The glasses you picked up",
        "allow you to see the floor here",
    ],
    "BlackHoleInfo": [
        "With this miniature worm hole,",
        "you feel like you could walk",
        "through walls, and leave",
    ],
    "ChooseWisely": [
        "You have but 1 life",
        "Choose your path wisely",
    ],
    "NatureHint": [
        "The Mother takes care",
        "of her baby son",
    ],
    "ElementsHint": [
        "The Father teaches him",
        "the way of the world",
    ],
    "SpiritHint": [
        "The Priest ushers him",
        "to the afterlife",
    ],
    "RealityHint": [
        "But was it all...",
        "..just a dream?",
    ],
    "DoorBlocked": [
        "The door behind shut behind you",
        "It will not budge",
    ],
    "Lore": [
        "The Trials of the Spires",
        "Those Unworthy will Perish",
    ],
    "Goats": [
        "Are those goat skeletons?",
        "How peculiar.",
    ],
    "NatureRoom": [
        "Nature",
        "Beyond the portal you see",
        "hints of plant life",
    ],
    "RealityRoom": [
        "Reality",
        "Beyond the portal you see",
        "Flashing and Shifting Colors",
    ],
    "SpiritRoom": [
        "Spirit",
        "Beyond the portal you see",
        "nothing but darkness",
    ],
    "ElementsRoom": [
        "Elements",
        "You feel heat eminating",
        "from beyond the portal",
    ],
    "Lose": [
        "You Died.",
        "Refresh to play again."
    ],
    "Thanks": [
        "You Win!",
        "Thanks for Playing",
    ]
}


def scene_to_phaser(python_scene_class):
    '''Phaser scenes use javascript "Classes"
        and methods make heavy use of the "this" keyword.
        This is a pain in the neck to use in Python,
        so we're wrapping that to capture "this"
        and keep it updated on a python object.
    '''

    scene = None

    def init(data):
        nonlocal scene
        context = javascript.this()
        scene = python_scene_class(context, data)

    def preload():
        nonlocal scene
        scene.context = javascript.this()
        console.log(f'Preload {scene.name}')
        scene.preload()

    def create(data):
        nonlocal scene
        scene.context = javascript.this()
        console.log(f'Create {scene.name}')
        scene.create(data)

    def update(time, delta):
        nonlocal scene
        scene.context = javascript.this()
        scene.update(time, delta)

    return {
        "init": init,
        "preload": preload,
        "create": create,
        "update": update,
    }


class PlantTrap:

    def __init__(self, scene, trigger, plant, sprite):
        self.scene = scene
        self.trigger = trigger
        self.plant = plant
        self.sprite = sprite
        self.alive = True
        self.triggered = False
        self.sprite.anims.play("plant")
        self.emitter = self.scene.particles.createEmitter({
            "frame": 237,
            "blendMode": "NORMAL",
            "speed": 250,
            "quantity": 20,
            "emitZone": {
                "type": "random",
                "source": Phaser.Geom.Circle.new(
                    trigger["x"],
                    trigger["y"],
                    3
                )
            },
            "deathZone": {
                "type": "onLeave",
                "source": Phaser.Geom.Circle.new(
                    trigger["x"],
                    trigger["y"],
                    45
                ),
            },
            "scale": {
                "start": 1,
                "end": 0.1
            }
        })
        self.emitter.stop()

    @classmethod
    def from_scene(cls, scene, o):
        x, y, w, h = o["x"], o["y"], o["width"], o["height"]
        trigger_sprite = scene.trigger_group.create(x + w / 2, y + h / 2)
        trigger_sprite.setSize(w, h)
        trigger_sprite.visible = False
        p = scene.find_plant(o)
        plant_sprite = scene.plants_group.create(p["x"], p["y"], "NPCs")
        result = cls(scene, trigger_sprite, p, plant_sprite)
        scene.context.physics.add.overlap(
            scene.player,
            trigger_sprite,
            result.trigger_trap
        )

        return result

    def trigger_trap(self, *args):
        if not self.triggered:
            self.triggered = True
            self.sprite.anims.play("explode")
            self.scene.context.time.addEvent({
                "delay": 600,
                "callback": self.explode,
                "callbackScope": self.scene.context,
                "loop": False
            })

    def explode(self, *args):
        self.emitter.start()
        spawn = self.plant
        target = self.scene.player
        x1, y1, x2, y2 = spawn["x"], spawn["y"], target["x"], target["y"]
        xd = x2 - x1
        yd = y2 - y1
        distance = math.sqrt(xd * xd + yd * yd)
        if distance < 40:
            self.damage_player()
        self.scene.context.time.addEvent({
            "delay": 120,
            "callback": lambda *args: self.emitter.stop(),
            "callbackScope": self.scene.context,
            "loop": False
        })

    def damage_player(self, *args):
        self.scene.damage_player()

class Braziers:

    def __init__(self):
        self.braziers = []

    def __iter__(self):
        yield from self.braziers

    @property
    def complete(self):
        return all(b.complete for b in self)

    def append(self, item):
        self.braziers.append(item)

    def move_ghosts_at_target(self):
        for brazier in self:
            if brazier.complete:
                continue
            elif brazier.lit:
                brazier.move_at_target(brazier.brazier)
            else:
                brazier.move_at_target(brazier.player)

    def return_ghosts(self):
        for brazier in self:
            if not brazier.complete:
                brazier.move_at_target(brazier.brazier)


class Brazier:

    def __init__(self, scene, player, brazier, ghost):
        self.scene = scene
        self.player = player
        self.brazier = brazier
        self.ghost = ghost
        self.lit = False
        self.complete = False
        self.ghost.anims.play("ghost")
        self.emitter = None

    def damage_player(self, *args):
        self.scene.damage_player()

    def collect_ghost(self, *args):
        if self.lit:
            self.lit = False
            self.complete = True
            self.ghost.destroy()
            self.emitter.stop()

    def move_at_target(self, target):
        angle = Phaser.Math.Angle.BetweenPoints(self.ghost, target)
        velocity = self.scene.context.physics.velocityFromRotation(angle, 50)
        self.ghost.setVelocity(velocity.x, velocity.y)

    def light(self, *args):
        if self.complete:
            return
        if self.lit:
            return
        if not self.scene.items.picked_up_flame:
            return
        self.lit = True
        zone = Phaser.Geom.Circle.new(self.brazier.x, self.brazier.y, 4)
        self.emitter = self.scene.particles.createEmitter({
            "frame": 224,
            "blendMode": "ADD",
            "speed": 4,
            "quantity": 0.5,
            "emitZone": {
                "type": "random",
                "source": zone,
            },
            "scale": {
                "start": 1,
                "end": 0.1
            }
        })

    @classmethod
    def from_scene(cls, scene, o):
        x, y, w, h = o["x"], o["y"], o["width"], o["height"]
        brazier = scene.trigger_group.create(x + w / 2, y + h / 2)
        brazier.setSize(w, h)
        brazier.visible = False
        g = scene.find_ghost(o)
        ghost = scene.ghosts_group.create(g["x"], g["y"], "NPCs")
        result = cls(scene, scene.player, brazier, ghost)
        context = scene.context
        context.physics.add.overlap(scene.player, ghost, result.damage_player)
        context.physics.add.overlap(scene.player, brazier, result.light)
        context.physics.add.overlap(brazier, ghost, result.collect_ghost)
        return result


class Golems:

    def __init__(self):
        self.golems = []

    def __iter__(self):
        yield from self.golems

    def append(self, item):
        self.golems.append(item)


class Golem:

    def __init__(self, scene, player, spawn, target, sprite):
        self.scene = scene
        self.player = player
        self.spawn = spawn
        self.target = target
        self.sprite = sprite
        self.sprite.anims.play("golem")
        x1, y1, x2, y2 = spawn["x"], spawn["y"], target["x"], target["y"]
        xd = x2 - x1
        yd = y2 - y1
        distance = math.sqrt(xd * xd + yd * yd)
        scene.context.tweens.timeline({
            "targets": self.sprite,
            "loop": -1,
            "tweens": [
                {
                    "x": self.target["x"],
                    "y": self.target["y"],
                    "duration": distance * 20,
                    "ease": "linear"
                },
                {
                    "x": self.spawn["x"],
                    "y": self.spawn["y"],
                    "duration": distance * 20,
                    "ease": "linear"
                },
            ]
        })

    def damage_player(self, *args):
        self.scene.damage_player()

    @classmethod
    def from_scene(cls, scene, spawn):
        target = scene.get_target(spawn)
        sprite = scene.golem_group.create(spawn["x"], spawn["y"])
        sprite.body.immovable = True
        sprite.setSize(28, 36, False)
        sprite.setOffset(2, 6)
        result = cls(scene, scene.player, spawn, target, sprite)
        context = scene.context
        context.physics.add.collider(
            scene.player,
            sprite,
            result.damage_player
        )
        return result


class Items:

    def __init__(self):
        self.totem = None
        self.flame = None
        self.glasses = None
        self.black_hole = None

    @property
    def picked_up_totem(self):
        return self.totem.picked_up

    @property
    def picked_up_flame(self):
        return self.flame.picked_up

    @property
    def picked_up_glasses(self):
        return self.glasses.picked_up

    @property
    def picked_up_black_hole(self):
        return self.black_hole.picked_up

    def append(self, item):
        if item.name == "Totem":
            self.totem = item
        elif item.name == "Everburning Flame":
            self.flame = item
        elif item.name == "Glasses":
            self.glasses = item
        elif item.name == "Black Hole":
            self.black_hole = item
        else:
            raise ValueError(f"Unknown item {item.name}")

    def show_glasses(self):
        self.glasses.show()

class Item:

    def __init__(self, name, sprite):
        self.sprite = sprite
        self.name = name
        self.picked_up = False
        self.sprite.anims.play(self.name)

    @classmethod
    def from_scene(cls, scene, o):
        context = scene.context
        properties = {p["name"]: p["value"] for p in o["properties"]}
        context.anims.create({
            "key": o["name"],
            "frames": context.anims.generateFrameNumbers("tilesheet", {
                "start": int(properties["start"]),
                "end": int(properties["end"])
            }),
            "frameRate": 10,
            "repeat": -1
        })

        sprite = context.physics.add.sprite(o["x"], o["y"], "tilesheet")
        if not o["visible"]:
            sprite.visible = False
        result = cls(o["name"], sprite)
        context.physics.add.overlap(scene.player, sprite, result.pick_up)
        return result

    def show(self):
        self.sprite.visible = True

    def pick_up(self, *args):
        if not self.picked_up and self.sprite.visible:
            self.sprite.visible = False
            self.picked_up = True


class GamePlayScene:

    def __init__(self, context, data):
        self.context = context
        self.data = data
        self.damage = 0
        self.bounds = ""
        self.plants = []
        self.items = Items()
        self.braziers = Braziers()
        self.golems = Golems()
        self.spirit_complete = False
        self.elements_complete = False
        self.invulnerable = 0

    @property
    def name(self):
        return self.__class__.__name__

    def preload(self):
        context = self.context
        context.load.image("tiles", "images/main-texture-extruded.png")
        context.load.spritesheet('tilesheet', 'images/main-texture.png', {
            "frameWidth": 16,
            "frameHeight": 16
        })
        context.load.spritesheet('player', 'images/player.png', {
            "frameWidth": 16,
            "frameHeight": 24
        })
        context.load.spritesheet('golem', 'images/golem.png', {
            "frameWidth": 32,
            "frameHeight": 48
        })
        context.load.spritesheet('NPCs', 'images/NPCs.png', {
            "frameWidth": 16,
            "frameHeight": 24
        })
        context.load.tilemapTiledJSON("map", "maps/map.json")
        context.load.bitmapFont(
            "bitmapfont",
            "fonts/bitmapfont.png",
            "fonts/bitmapfont.xml"
        )
        context.load.scenePlugin({
            "key": "AnimatedTiles",
            "url": "js/AnimatedTiles.js",
            "sceneKey": "AnimatedTiles"
        })

    def get_target(self, o):
        properties = {p["name"]: p["value"] for p in o["properties"]}
        target = properties["target"]
        return self.objects[target]

    def find_plant(self, trigger):
        return self.get_target(trigger)

    def find_ghost(self, brazier):
        return self.get_target(brazier)

    def process_door(self, door):
        destination = self.get_target(door)
        self.player.x = destination["x"]
        self.player.y = destination["y"]

    def move_camera(self, bounds):
        if self.bounds != bounds["name"]:
            self.camera.setBounds(
                bounds["x"],
                bounds["y"],
                bounds["width"],
                bounds["height"]
            )
            self.bounds = bounds["name"]
            if self.bounds == "RealityBounds":
                if self.items.picked_up_glasses:
                    self.walls.visible = True
                    self.walls_over.visible = True
                else:
                    self.walls.visible = False
                    self.walls_over.visible = False
            else:
                self.walls.visible = True
                self.walls_over.visible = True

    def set_text(self, message_name):
        if message_name == "BlackHoleinfo":
            if not self.items.picked_up_black_hole:
                self.text.setText([""])
                return
        elif message_name == "GlassesInfo":
            if not self.items.picked_up_glasses:
                self.text.setText([""])
                return
        elif message_name == "FlameInfo":
            if not self.items.picked_up_flame:
                self.text.setText([""])
                return
        elif message_name == "TotemInfo":
            if not self.items.picked_up_totem:
                self.text.setText([""])
                return
        messages = MESSAGES.get(message_name, [""])
        self.text.setText(messages)

    def damage_player(self):
        if self.invulnerable <= 0:
            self.damage += 1
            self.invulnerable = 30
            for i, heart in enumerate(self.hearts):
                if i == 5 - self.damage:
                    heart.anims.play("damage-heart")
            if self.damage >= 5:
                self.lose()

    def move_enemies(self):
        if self.bounds == "SpiritBounds":
            self.braziers.move_ghosts_at_target()
        else:
            self.braziers.return_ghosts()

    def win(self):
        if self.items.picked_up_black_hole:
            win = self.objects[113]
            self.player.x = win["x"]
            self.player.y = win["y"]

    def lose(self):
        lose = self.objects[114]
        self.player.x = lose["x"]
        self.player.y = lose["y"]

    def move_player(self):
        speed = 100

        if (self.cursors.left.isDown):
            dx = -speed
        elif (self.cursors.right.isDown):
            dx = speed
        else:
            dx = 0.0

        if (self.cursors.up.isDown):
            dy = -speed
        elif (self.cursors.down.isDown):
            dy = speed
        else:
            dy = 0.0

        if dy < 0.0:
            self.player.anims.play('up', True)
        elif dy > 0.0:
            self.player.anims.play('down', True)
        elif dx < 0.0:
            self.player.anims.play('left', True)
        elif dx > 0.0:
            self.player.anims.play('right', True)
        else:
            self.player.anims.stop()

        if dx != 0.0 and dy != 0.0:
            dx, dy = dx * 0.707, dy * 0.707

        self.player.setVelocityX(dx)
        self.player.setVelocityY(dy)

    def update(self, time, delta):
        self.move_player()
        self.move_enemies()
        self.detect_success()
        self.update_text()
        self.invulnerable -= 1

    def update_text(self):
        if not self.context.physics.overlap(self.player, self.text_group):
            self.text.visible = False
        else:
            self.text.visible = True

    def detect_success(self):
        if not self.spirit_complete and self.braziers.complete:
            self.spirit_complete = True
            self.items.show_glasses()
        if not self.elements_complete and self.items.picked_up_flame:
            self.elements_complete = True
            self.particles.createEmitter({
                "frame": 224,
                "blendMode": "ADD",
                "speed": 4,
                "quantity": 2,
                "follow": self.player,
                "scale": {
                    "start": 1,
                    "end": 0.1
                }
            })

    def create_flame_emitter(self, x, y, w, h):
        zone = Phaser.Geom.Rectangle.new(x, y, w, h)
        return self.particles.createEmitter({
            "frame": 224,
            "blendMode": "ADD",
            "speed": 4,
            "quantity": 2,
            "emitZone": {
                "type": "random",
                "source": zone,
            },
            "scale": {
                "start": 1,
                "end": 0.1
            }
        })

    def create_animations(self):
        context = self.context

        context.anims.create({
            "key": "golem",
            "frames": context.anims.generateFrameNumbers('golem', {
                "start": 0,
                "end": 15
            }),
            "frameRate": 10,
            "repeat": -1
        })

        context.anims.create({
            "key": "plant",
            "frames": context.anims.generateFrameNumbers('NPCs', {
                "start": 20,
                "end": 21
            }),
            "frameRate": 10,
        })

        context.anims.create({
            "key": "ghost",
            "frames": context.anims.generateFrameNumbers('NPCs', {
                "start": 5,
                "end": 9
            }),
            "frameRate": 10,
        })

        context.anims.create({
            "key": "explode",
            "frames": context.anims.generateFrameNumbers('NPCs', {
                "start": 21,
                "end": 24
            }),
            "frameRate": 5,
        })

        context.anims.create({
            "key": "stand",
            "frames": context.anims.generateFrameNumbers('player', {
                "frame": 0
            }),
            "frameRate": 20,
        })

        context.anims.create({
            "key": "down",
            "frames": context.anims.generateFrameNumbers('player', {
                "start": 4,
                "end": 7
            }),
            "frameRate": 10,
            "repeat": -1
        })

        context.anims.create({
            "key": "up",
            "frames": context.anims.generateFrameNumbers('player', {
                "start": 8,
                "end": 11
            }),
            "frameRate": 10,
            "repeat": -1
        })

        context.anims.create({
            "key": "right",
            "frames": context.anims.generateFrameNumbers('player', {
                "start": 12,
                "end": 15
            }),
            "frameRate": 10,
            "repeat": -1
        })

        context.anims.create({
            "key": "left",
            "frames": context.anims.generateFrameNumbers('player', {
                "start": 16,
                "end": 19
            }),
            "frameRate": 10,
            "repeat": -1
        })

        context.anims.create({
            "key": "flame",
            "frames": context.anims.generateFrameNumbers("tilesheet", {
                "start": 224,
                "end": 233
            }),
            "frameRate": 10,
            "repeat": -1
        })

        context.anims.create({
            "key": "full-heart",
            "frames": context.anims.generateFrameNumbers("tilesheet", {
                "start": 320,
                "end": 320
            }),
            "frameRate": 10,
            "repeat": -1
        })

        context.anims.create({
            "key": "damage-heart",
            "frames": context.anims.generateFrameNumbers("tilesheet", {
                "start": 320,
                "end": 322
            }),
            "frameRate": 10,
            "repeat": 2
        })

    def parse_tilemap(self):
        context = self.context
        self.player = context.physics.add.sprite(0, 0, "player")
        self.player.setSize(14, 8, False)
        self.player.setOffset(1, 16)
        tilemap = context.make.tilemap({"key": "map"})
        tileset = tilemap.addTilesetImage("main-texture", "tiles")
        floors = tilemap.createDynamicLayer("Floor", tileset, 0, 0)
        self.walls = tilemap.createDynamicLayer("Walls", tileset, 0, 0)
        decor = tilemap.createDynamicLayer("Decor", tileset, 0, 0)
        self.walls_over = tilemap.createDynamicLayer("WallsOverlay", tileset, 0, 0)
        decor_over = tilemap.createDynamicLayer("DecorOverlay", tileset, 0, 0)
        cLayer = tilemap.createDynamicLayer("Collision", tileset, 0, 0)
        context.AnimatedTiles.init(tilemap)
        floors.depth = -2
        self.walls.depth = -2
        decor.depth = -2
        self.player.depth = -1
        self.walls_over.depth = 0
        decor_over.depth = 0
        tilemap.setCollisionByExclusion([1024])
        self.objects = {
            o["id"]: o for o in
            tilemap.getObjectLayer('Objects')['objects']
        }
        context.physics.add.collider(self.player, cLayer)
        return tilemap

    def create(self, data):
        self.cursors = self.context.input.keyboard.createCursorKeys()
        self.trigger_group = self.context.physics.add.staticGroup()
        self.create_animations()
        self.parse_tilemap()
        self.create_player()
        self.create_doors()
        self.create_text()
        self.create_camera_bounds()
        self.create_emitters()
        self.create_plants()
        self.create_braziers()
        self.create_golems()
        self.create_items()
        self.create_hearts()
        self.create_win()

    def create_win(self):
        for o in self.objects.values():
            if o["type"] == "Win":
                x, y, w, h = o["x"], o["y"], o["width"], o["height"]
                win_sprite = self.trigger_group.create(x + w / 2, y + h / 2)
                win_sprite.setSize(w, h)
                win_sprite.visible = False
                self.context.physics.add.overlap(
                    self.player,
                    win_sprite,
                    lambda *args, o=o: self.win()
                )

    def create_plants(self):
        self.plants_group = self.context.physics.add.group()
        for o in self.objects.values():
            if o["type"] == "PlantTrigger":
                self.plants.append(PlantTrap.from_scene(self, o))

    def create_doors(self):
        self.doors_group = self.context.physics.add.staticGroup()
        for o in self.objects.values():
            if o["type"] == "Door":
                x, y, w, h = o["x"], o["y"], o["width"], o["height"]
                door_sprite = self.doors_group.create(x + w / 2, y + h / 2)
                door_sprite.setSize(w, h)
                door_sprite.visible = False
                self.context.physics.add.overlap(
                    self.player,
                    door_sprite,
                    lambda *args, o=o: self.process_door(o)
                )

    def create_hearts(self):
        self.hearts = []
        for i in range(5):
            heart = self.context.add.sprite(i * 12 + 12, 12, "tiles")
            heart.depth = 3
            heart.setScrollFactor(0, 0)
            heart.anims.play("full-heart")
            self.hearts.append(heart)

    def create_text(self):
        self.text = self.context.add.bitmapText(
            128, 36, 'bitmapfont', '', 7, 1
        ).setOrigin(0.5).setScrollFactor(0, 0)
        self.text.lineSpacing = 5
        self.text.depth = 3

        self.text.setText([
            'Arrow Keys to Move.',
        ])
        self.text_group = self.context.physics.add.staticGroup()
        for o in self.objects.values():
            if o["type"] == "Text":
                x, y, w, h = o["x"], o["y"], o["width"], o["height"]
                show_text_sprite = self.text_group.create(x + w / 2, y + h / 2)
                show_text_sprite.setSize(w, h)
                show_text_sprite.visible = False
                self.context.physics.add.overlap(
                    self.player,
                    show_text_sprite,
                    lambda *args, o=o: self.set_text(o["name"])
                )

    def create_camera_bounds(self):
        self.bounds_group = self.context.physics.add.staticGroup()
        for o in self.objects.values():
            if o["type"] == "Camera":
                x, y, w, h = o["x"], o["y"], o["width"], o["height"]
                object_sprite = self.bounds_group.create(x + w / 2, y + h / 2)
                object_sprite.setSize(w, h)
                object_sprite.visible = False
                self.context.physics.add.overlap(
                    self.player,
                    object_sprite,
                    lambda *args, o=o: self.move_camera(o)
                )
        self.camera = self.context.cameras.main
        self.camera.startFollow(self.player, True, 1, 1)

    def create_braziers(self):
        self.ghosts_group = self.context.physics.add.group()
        for o in self.objects.values():
            if o["type"] == "Brazier":
                self.braziers.append(Brazier.from_scene(self, o))
        self.context.physics.add.collider(self.ghosts_group, self.ghosts_group)

    def create_emitters(self):
        self.damage_group = self.context.physics.add.staticGroup()
        self.particles = self.context.add.particles('tilesheet')
        self.particles.depth = -2
        for o in self.objects.values():
            if o["type"] == "Emitter":
                x, y, w, h = o["x"], o["y"], o["width"], o["height"]
                self.create_flame_emitter(x, y, w, h)
                damage_sprite = self.damage_group.create(x + w / 2, y + h / 2)
                damage_sprite.setSize(w, h)
                damage_sprite.visible = False
                self.context.physics.add.overlap(
                    self.player,
                    damage_sprite,
                    lambda *args, o=o: self.walk_coals()
                )

    def walk_coals(self):
        if self.items.picked_up_totem:
            return
        else:
            self.damage_player()

    def create_items(self):
        for o in self.objects.values():
            if o["type"] == "Item":
                self.items.append(Item.from_scene(self, o))

    def create_player(self):
        for o in self.objects.values():
            if o["name"] == 'PlayerSpawn':
                self.player.x = o["x"]
                self.player.y = o["y"]

    def create_golems(self):
        self.golem_group = self.context.physics.add.group()
        for o in self.objects.values():
            if o["type"] == "GolemSpawn":
                self.golems.append(Golem.from_scene(self, o))


game = Phaser.Game.new({
    "type": Phaser.AUTO,
    "width": 256,
    "height": 224,
    "pixelArt": True,
    "zoom": 3,
    "physics": {
        "default": "arcade",
        "arcade": {
            "debug": False,
        },
    },
})

game.scene.add("GamePlayScene", scene_to_phaser(GamePlayScene))
game.scene.start("GamePlayScene")
